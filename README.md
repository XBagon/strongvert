```sh
strongvert <target-path>
```

Tries to convert all files in <target-path> from TGX to PNG image format and outputs them in `./out/`.
