use byteorder::{LittleEndian, ReadBytesExt};
use image::{ImageBuffer, RgbaImage};
use rayon::prelude::*;
use std::{
    fs::{self, File},
    io::{Read, Seek, SeekFrom},
    path::{Path},
};
#[macro_use]
extern crate derive_more;

#[derive(Debug)]
#[derive(Display)]
enum Error {
    Custom(&'static str),
    ParsingError,
    Image,
    IO(std::io::Error),
    EOF,
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Error::IO(err)
    }
}

struct MetaData {
    width: u16,
    height: u16,
}

impl MetaData {
    fn parse<R: Read + Seek>(r: &mut R) -> Result<Self, Error> {
        let width = r.read_u16::<LittleEndian>()? + 1;
        r.seek(SeekFrom::Current(2))?;
        let height = r.read_u16::<LittleEndian>()? + 1;
        r.seek(SeekFrom::Current(2))?;

        Ok(Self {
            width,
            height,
        })
    }
}

#[derive(Debug)]
struct TGX {
    width: u16,
    height: u16,
    tokens: Vec<Token>,
}

impl TGX {
    pub fn from<R: Read + Seek>(mut r: R) -> Result<Self, Error> {
        let metadata = MetaData::parse(&mut r)?;
        let mut tokens = Vec::new();
        loop {
            match Token::parse(&mut r) {
                Ok(token) => {
                    tokens.push(token);
                }
                Err(Error::EOF) => {
                    break;
                }
                e => {
                    e?;
                }
            }
        }

        Ok(Self {
            width: metadata.width,
            height: metadata.height,
            tokens,
        })
    }

    pub fn to_rgba_image(&self, cnv_fn: fn(&Color) -> [u8; 4]) -> Result<RgbaImage,Error> {
        let mut image_buf = TGXBuffer::new(cnv_fn);
        for token in &self.tokens {
            match token {
                Token::PixelStream(ps) => image_buf.push_pixel_stream(ps),
                Token::Newline => {
                    let x = image_buf.buf.len() / 4 % (self.width) as usize;
                    for _ in x..self.width as usize - 1 {
                        image_buf.push_transparent();
                    }
                }
                Token::RepeatingPixels(rp) => image_buf.push_reapeating_pixels(rp),
                Token::TransparentPixelString(tps) => {
                    for _ in 0..tps.length {
                        image_buf.push_transparent();
                    }
                }
            }
        }
        let remaining = (self.width as usize * self.height as usize) - image_buf.buf.len() / 4;
        for _ in 0..remaining {
            image_buf.push_transparent();
        }
        ImageBuffer::from_vec(self.width as u32, self.height as u32, image_buf.buf).ok_or(Error::Image)
    }
}

#[derive(Debug)]
struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

impl Color {
    fn parse<R: Read + Seek>(r: &mut R) -> Result<Self, Error> {
        let mut buf = [0u8; 2];
        r.read_exact(&mut buf)?;
        let mut green = (buf[0] & 0b11100000) >> 5;
        let blue = buf[0] & 0b00011111;
        let red = (buf[1] & 0b01111100) >> 2;
        green |= (buf[1] & 0b00000011) << 3;

        Ok(Self {
            red,
            green,
            blue,
        })
    }
}

#[derive(Debug)]
struct PixelStream {
    pixels: Vec<Color>,
}

impl PixelStream {
    fn parse<R: Read + Seek>(mut r: &mut R, length: u8) -> Result<Self, Error> {
        let mut stream = PixelStream {
            pixels: Vec::with_capacity(length as usize),
        };
        for _ in 0..length {
            stream.pixels.push(Color::parse(&mut r)?);
        }
        Ok(stream)
    }
}

#[derive(Debug)]
struct RepeatingPixels {
    length: u8,
    pixel: Color,
}

impl RepeatingPixels {
    fn parse<R: Read + Seek>(mut r: &mut R, length: u8) -> Result<Self, Error> {
        let pixel = Color::parse(&mut r)?;
        Ok(Self {
            length,
            pixel,
        })
    }
}

#[derive(Debug)]
struct TransparentPixelString {
    length: u8,
}

impl TransparentPixelString {
    fn new(length: u8) -> Result<Self, Error> {
        Ok(Self {
            length,
        })
    }
}

#[derive(Debug)]
enum Token {
    PixelStream(PixelStream),
    Newline,
    RepeatingPixels(RepeatingPixels),
    TransparentPixelString(TransparentPixelString),
}

impl Token {
    fn parse<R: Read + Seek>(mut r: &mut R) -> Result<Self, Error> {
        let mut buf = [0u8; 1];
        match r.read_exact(&mut buf) {
            Err(ref e) if e.kind() == std::io::ErrorKind::UnexpectedEof => return Err(Error::EOF),
            e => e?,
        };
        match buf[0] >> 5 {
            0b000 => {
                let length = (buf[0] & 0b00011111) + 1;
                PixelStream::parse(&mut r, length).map(Token::PixelStream)
            }
            0b100 => Ok(Token::Newline),
            0b010 => {
                let length = (buf[0] & 0b00011111) + 1;
                RepeatingPixels::parse(&mut r, length).map(Token::RepeatingPixels)
            }
            0b001 => {
                let length = (buf[0] & 0b00011111) + 1;
                TransparentPixelString::new(length).map(Token::TransparentPixelString)
            }
            _ => Err(Error::ParsingError),
        }
    }
}

struct TGXBuffer {
    buf: Vec<u8>,
    cnv_fn: fn(&Color) -> [u8; 4],
}

impl TGXBuffer {
    pub fn new(cnv_fn: fn(&Color) -> [u8; 4]) -> Self {
        Self {
            buf: Vec::new(),
            cnv_fn,
        }
    }

    pub fn push_color_data(&mut self, color_data: [u8; 4]) {
        self.buf.push(color_data[0]);
        self.buf.push(color_data[1]);
        self.buf.push(color_data[2]);
        self.buf.push(color_data[3]);
    }

    pub fn push_pixel_stream(&mut self, pixel_stream: &PixelStream) {
        for pixel in &pixel_stream.pixels {
            let color_data = (self.cnv_fn)(&pixel);
            self.push_color_data(color_data);
        }
    }

    pub fn push_reapeating_pixels(&mut self, repeating_pixels: &RepeatingPixels) {
        let color_data = (self.cnv_fn)(&repeating_pixels.pixel);
        for _ in 0..repeating_pixels.length {
            self.push_color_data(color_data)
        }
    }

    pub fn push_transparent(&mut self) {
        self.buf.push(0);
        self.buf.push(0);
        self.buf.push(0);
        self.buf.push(0);
    }
}

fn run_app() -> Result<(), Error> {
    let mut args = std::env::args();
    args.next().unwrap();
    let path = if let Some(path) = args.next() {
        path
    } else {
        Err(Error::Custom("Must provide path to asset folder as argument!"))?
    };

    let asset_folder = Path::new(&path);
    if !asset_folder.exists() {
        Err(Error::Custom("Must provide path to existing asset folder as argument!"))?
    }

    let out_dir = Path::new("out\\");
    if !out_dir.exists() {
        fs::create_dir(out_dir)?;
    }
    fs::read_dir(asset_folder)?.collect::<Vec<_>>().into_par_iter().map::<_,Result<(), Error>>(|entry| {
        let entry = entry?;
        let file = File::open(entry.path())?;
        let tgx = TGX::from(file)?;
        let out = tgx.to_rgba_image(
            |c| [(c.red as f32 * (255.0 / 31.0)) as u8, (c.green as f32 * (255.0 / 31.0)) as u8, (c.blue as f32 * (255.0 / 31.0)) as u8, 255]
        )?;
        out.save(Path::new("out\\").join(Path::new(&entry.file_name())).with_extension("png"))?;
        Ok(())
    }).collect::<Result<Vec<_>,_>>()?;

    Ok(())
}

fn main() {
    std::process::exit(match run_app() {
        Ok(_) => 0,
        Err(err) => {
            eprintln!("Error: {}", err);
            1
        }
    });
}